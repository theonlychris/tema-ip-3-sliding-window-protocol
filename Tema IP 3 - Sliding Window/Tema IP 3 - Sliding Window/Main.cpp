#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>
#include "Utils.hpp"

using Random = effolkronium::random_static;

class Frame
{
public:
	Frame(int number = -1, const std::string& value = Utils::getRandomString(10), int willNotReachDestination = 0);
	~Frame() = default;

public:
	int number;
	std::string value;
	bool willNotReachDestination;
public:
	struct Comparator
	{
		bool operator()(const Frame& frameOne, const Frame& frameTwo) const
		{
			return frameOne.number < frameTwo.number;
		}
	};

public:
	friend std::ostream& operator << (std::ostream& os, const Frame& frame);
};

Frame::Frame(int number, const std::string & value, int willNotReachDestination) : number(number), value(value), willNotReachDestination(willNotReachDestination)
{
}

std::ostream& operator<<(std::ostream& os, const Frame& frame)
{
	os << "[" << frame.number << ", " << frame.value << ", " << frame.willNotReachDestination << "]";
	return os;
}

void tryToSend(Frame& frameToSend, std::vector<Frame>& receiver) 
{
	if (frameToSend.willNotReachDestination == 0)
	{
	std::cout << "ACK FRAME " << frameToSend.number << std::endl;
	receiver.push_back(frameToSend);
	}
	else
	{
		std::cout << "RESEND FRAME " << frameToSend.number << std::endl;
	}
}

int main()
{ 
	/* --------------------------------- RANDOM PROGRAM  ---------------------------------*/
	/*
	int senderSize;
	std::cout << "Enter the sender size: ";
	std::cin >> senderSize;
	std::cout << "Enter how many frames would you like to not be send in the first try: ";
	int howManyWillNotBeSent;
	std::cin >> howManyWillNotBeSent;

	std::vector<Frame> sender(senderSize);
	for (int i = 0; i < senderSize; ++i)
	{
		sender[i].number = i;
	}
	std::vector<Frame> receiver;
	receiver.reserve(sender.size());

	for (int i = 0; i < howManyWillNotBeSent; ++i)
	{	
		auto getRandom = Random::get(0, static_cast<int>(sender.size() - 1));
		if (sender[getRandom].willNotReachDestination == 1)
			howManyWillNotBeSent++;
		else
		sender[getRandom].willNotReachDestination = 1;
	}
	*/
	/* --------------------------------- RANDOM PROGRAM  ---------------------------------*/
	
	/* --------------------------------- TEST PROGRAM  ---------------------------------*/
	
	std::vector<Frame> sender = { {0,"one",1}, {1,"two",0}, {2,"three",0}, {3,"four",1}, {4,"five",0}, {5,"six",1}, {6,"seven",0}, {7,"eight",1} };
	std::vector<Frame> receiver;
	receiver.reserve(sender.size());
	
	/* --------------------------------- TEST PROGRAM  ---------------------------------*/
	
	std::cout << sender;
	

	int currentIndex = 0;

	int groupSize = 3;
	std::queue<Frame> queue;

	bool notReached = 0;

	while (currentIndex < sender.size())
	{
		groupSize = 3;
		if (!queue.empty())
		{
			groupSize -= queue.size();
		}

		for (int i = currentIndex; i < currentIndex + groupSize; ++i)
		{
			if (i >= sender.size())
				break;

			queue.push(sender[i]);
		}
		while (queue.size())
		{
			tryToSend(queue.front(), receiver);
	
			if (queue.front().willNotReachDestination == 1)
			{
				queue.front().willNotReachDestination = 0;
				queue.push(queue.front());
				notReached = 1;
			}
			queue.pop();
		}
		std::cout << receiver;

		if (notReached == 1)
		{
		std::sort(receiver.begin(), receiver.end(), Frame::Comparator());
		}

		std::cout << receiver;
		
		notReached = 0;
		currentIndex += groupSize;
	}
	std::cin.get();
	return 0;
}

