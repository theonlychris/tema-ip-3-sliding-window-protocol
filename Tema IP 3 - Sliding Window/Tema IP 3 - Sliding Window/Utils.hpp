#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>
#include "random.hpp"
using Random = effolkronium::random_static;

/* Overloads the << Operator to show the contents of a vector */
template <typename T>
std::ostream& operator << (std::ostream& os, const std::vector<T>& myVector)
{
	for (int i = 0; i < myVector.size(); ++i) {
		os << myVector[i];
		if (i != myVector.size() - 1)
			os << std::endl;
	}
	os << std::endl << std::endl;
	return os;
}

namespace Utils
{
	/* Gets a random string */
	inline std::string getRandomString(int stringSize)
	{
		std::string characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		std::string randomString = "";
		int charSize = characters.size() - 1;
		for (int i = 0; i < stringSize; i++)
		{
			randomString += characters[Random::get(1,charSize)];
		}
		return randomString;
	}
}
#endif // !UTILS_HPP




